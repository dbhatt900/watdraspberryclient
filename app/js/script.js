$(function(){

  var messages = [];
  var peer_id, name, conn;
  var messages_template = Handlebars.compile($('#messages-template').html());
  var options = {
    'constraints': {
      'mandatory': {
        'OfferToReceiveAudio': 1,
        'OfferToReceiveVideo': 1
      }
    }
  };
  var peer = new Peer({
   host: 'watd.centurus.it',
   port: 9090,
   debug: 3,
   config: {'iceServers': [
   {url:'stun:numb.viagenie.ca'},
   { url: 'turn:numb.viagenie.ca',
   credential: 'Bhattdeepak_90', username: 'dbhatt900@gmail.com' }
   ]}
 });

  peer.on('open', function(){
    //$('#id').text(peer.id);
    console.log("peerId",peer.id);
    console.log("peer",peer);
  });

  navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

  function getVideo(callback){
    navigator.getUserMedia({audio: true, video: true}, callback, function(error){
      console.log(error);
      alert('An error occured. Please try again');
    });
  }

  getVideo(function(stream){
    window.localStream = stream;
    onReceiveStream(stream, 'my-camera');
  });

  function onReceiveStream(stream, element_id){
    console.log("stream",stream);

    var video = $('#' + element_id + ' video')[0];
    console.log("video",video)
    video.src = window.URL.createObjectURL(stream);
    console.log("video.src",video.src)
    window.peer_stream = stream;
    console.log("window.peer_stream ",window.peer_stream )
  }

  $('#login').click(function(){
    name = $('#name').val();
    peer_id = $('#peer_id').val();
    if(peer_id){
      conn = peer.connect(peer_id, {metadata: {
        'username': name
      }});
      conn.on('data', handleMessage);
      call_client();
    }

    $('#chat').removeClass('hidden');
    $('#connect').addClass('hidden');
  });

  

  peer.on('connection', function(connection){
    conn = connection;
    peer_id = connection.peer;
    conn.on('data', handleMessage);

    $('#peer_id').addClass('hidden').val(peer_id);
    $('#connected_peer_container').removeClass('hidden');
    $('#connected_peer').text(connection.metadata.username);
  });

  function handleMessage(data){
    var header_plus_footer_height = 285;
    var base_height = $(document).height() - header_plus_footer_height;
    var messages_container_height = $('#messages-container').height();
    messages.push(data);

    var html = messages_template({'messages' : messages});
    $('#messages').html(html);

    if(messages_container_height >= base_height){
      $('html, body').animate({ scrollTop: $(document).height() }, 500);
    }
  }

  function sendMessage(){
    var text = $('#message').val();
    var data = {'from': name, 'text': text};

    conn.send(data);
    handleMessage(data);
    $('#message').val('');
  }

  $('#message').keypress(function(e){
    if(e.which == 13){
      sendMessage();
    }
  });

  $('#send-message').click(sendMessage);

  $('#call').click(function(){
    console.log('now calling: ' + peer_id);
    console.log(peer);
    var call = peer.call(peer_id, window.localStream);
    call.on('stream', function(stream){
      window.peer_stream = stream;
      onReceiveStream(stream, 'peer-camera');
    });
  });

  function call_client(){
    console.log('now calling: ' + peer_id);
    console.log(peer);
    document.getElementById("peer-camera").style.display = "block";
    var call = peer.call(peer_id, window.localStream);
    call.on('stream', function(stream){
      window.peer_stream = stream;
      onReceiveStream(stream, 'peer-camera');
    });
  }

  peer.on('call', function(call){
    onReceiveCall(call);
  });

  function onReceiveCall(call){
    call.answer(window.localStream);
    document.getElementById("peer-camera").style.display = "block";

    call.on('stream', function(stream){
      window.peer_stream = stream;
      onReceiveStream(stream, 'peer-camera');
    });

    call.on('close',function(){
      console.log("inside close");
      onClose();
    });
    console.log("window.localStream",window.localStream.getAudioTracks());
    
  }

  function onClose(){
    window.close();
    return false;
  }

});
