var GPIO = require('onoff').Gpio,
    button = new GPIO(18, 'in', 'both');
 
// define the callback function
function ring(err, state) {
	if(err){
		console.log(err);
	}
	else{
		console.log("button pressed");
	}
  
}
 
// pass the callback function to the
// as the first argument to watch()
button.watch(ring);